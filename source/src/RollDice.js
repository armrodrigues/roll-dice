import React, { Component } from "react";
import Die from "./Die";
import "./RollDice.css";

class RollDice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            die1: "",
            die2: "",
            isRolling: false,
        };
        this.handleRoll = this.handleRoll.bind(this);
        this.roll = this.roll.bind(this);
    }

    roll() {
        let faces = ["one", "two", "three", "four", "five", "six"];
        let die1 = Math.floor(Math.random() * 6) + 0;
        let die2 = Math.floor(Math.random() * 6) + 0;
        this.setState({
            die1: faces[die1],
            die2: faces[die2],
        });
    }

    handleRoll() {
        let rolling = setInterval(this.roll, 300);

        setTimeout(() => {
            this.roll();
            this.setState({
                isRolling: false,
            });
            clearInterval(rolling);
        }, 1500);

        this.setState({
            isRolling: true,
        });
    }

    render() {
        let buttonText = "";
        if (!this.state.isRolling) {
            buttonText = "Roll Dice!";
        } else {
            buttonText = "Rolling!";
        }
        return (
            <div className="RollDice">
                <Die face={this.state.die1} isRolling={this.state.isRolling} />
                <Die face={this.state.die2} isRolling={this.state.isRolling} />
                <button
                    className="roll-btn"
                    onClick={this.handleRoll}
                    disabled={this.state.isRolling}>
                    {buttonText}
                </button>
            </div>
        );
    }
}

export default RollDice;
