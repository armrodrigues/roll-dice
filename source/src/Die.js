import React, {Component} from 'react';
import './Die.css';

class Die extends Component
{
    render ()
    {
        var dieFace = "";
        switch (this.props.face) {
            case 'one':
                dieFace = "fa fa-dice-one"
                break;
            case 'two':
                dieFace = "fa fa-dice-two"
                break;
            case 'three':
                dieFace = "fa fa-dice-three"
                break;
            case 'four':
                dieFace = "fa fa-dice-four"
                break;
            case 'five':
                dieFace = "fa fa-dice-five"
                break;
            case 'six':
                dieFace = "fa fa-dice-six"
                break;
            default:
                dieFace = "fa fa-dice-one"
                break;
        }
        var shakeClass = "";
        if(this.props.isRolling === true)
        {
            shakeClass = " rolling";
        } else
        {
            shakeClass = "";
        }
        return (
            <div className="Dice">
                <i className={dieFace + shakeClass}></i>
            </div>
        );
    }
}

export default Die;